from flask import Flask, jsonify, request
from neo4j import GraphDatabase
from dotenv import load_dotenv
import os #provides ways to access the Operating System and allows us to read the environment variables

load_dotenv()

app = Flask(__name__)

uri = os.getenv('URI')
user = os.getenv("USERNAME")
password = os.getenv("PASSWORD")
driver = GraphDatabase.driver("bolt://localhost:7687", auth=("neo4j", "test1234"),database="neo4j")

workers = [{"name":"Jan",
            "surname": "Kowalski",
            "position": "developer"},
           {"name":"Anna",
            "surname": "Enna",
            "position": "developer"},
           {"name":"Lena",
            "surname": "Renna",
            "position": "specialist"},
           {"name":"Helena",
            "surname": "Groch",
            "position": "specialist"},
           {"name":"Severyn",
            "surname": "Goździk",
            "position": "manager"}]
dep = [{"name":"ceny",
        "info": "Prices"},
       {"name":"design",
        "info": "Projects, look, colors"},
       {"name":"kod",
        "info":"developres, programmers, testers"},
       {"name":"planowanie",
        "info":"brain-storming, begining"}]
def add_data():
    with driver.session() as session:
        for worker in workers:
            session.write_transaction(add_new_employee, worker['name'], worker['surname'], worker['position'])

        for department in dep:
            session.write_transaction(add_department, department['name'], department['info'])

        session.write_transaction(add_WORKS, "Jan", "design")
        session.write_transaction(add_WORKS, "Lena", "design")
        session.write_transaction(add_WORKS, "Anna", "kod")
        session.write_transaction(add_WORKS, "Severyn", "ceny")
        session.write_transaction(add_MANAGES, "Helena", "ceny")
def add_WORKS(tx, employee_name, department_name):
    query = (
        "MATCH (e:employee {name: $employee_name}), (d:department {name: $department_name}) "
        "MERGE (e)-[:WORKS_IN]->(d)"
    )
    tx.run(query, employee_name=employee_name, department_name=department_name)

def add_MANAGES(tx, employee_name, department_name):
    query = (
        "MATCH (e:employee {name: $employee_name}), (d:department {name: $department_name}) "
        "MERGE (e)-[:MANAGES]->(d)"
    )
    tx.run(query, employee_name=employee_name, department_name=department_name)

def get_Employee(tx, prop=None, filter=None, sortBy=None):
    query = "MATCH (e:employee)"

    if prop and filter:
        a = f"e.{prop} = {filter}"
        query += " WHERE " +a+" "

    query += " RETURN e "
    if sortBy:
        query += f"ORDER BY e.{sortBy}"

    results = tx.run(query, prop=prop, filter=filter, sortBy=sortBy).data()
    employees = [{'name': result['e']['name'], 'surname': result['e']['surname'], 'position': result['e']['position']}
                 for result in results]
    return employees

@app.route('/employees', methods=['GET'])
def get_employee_route():
    prop = request.args.get('prop')
    filterr = request.args.get('filter')
    sortBy = request.args.get('sortBy')
    with driver.session() as session:
        employees = session.read_transaction(get_Employee, prop, filterr, sortBy)

    response = {'employees': employees}
    return jsonify(response)


def add_employee(tx, name, surname, position):
    query = "CREATE (e:employee {name: $name, surname: $surname, position: $position})"
    tx.run(query, name=name, surname=surname, position=position)


def add_new_employee(tx, name, surname, position, department):
    query = "MATCH (e:employee {name: $name, surname: $surname}) RETURN e"
    result = tx.run(query, name=name, surname=surname).data()
    if len(result) != 0:
        return {'status': 'not unique'}
    query = "CREATE (e:employee {name: $name, surname: $surname, position: $position})"
    tx.run(query, name=name, surname=surname, position=position)

    query = "MATCH (e:employee {name: $name, surname: $surname}), (d:department {name: $department}) " \
            "MERGE (e)-[:WORKS_IN]->(d)"
    tx.run(query, name=name, surname=surname, department=department)

    return {'status': 'success'}

@app.route('/employees', methods=['POST'])
def add_employee_route():
    try:
        name = request.json['name']
        surname = request.json['surname']
        position = request.json['position']
        department = request.json.get('department')
    except:
        response = {'status': 'error, all data must be provided'}
        return jsonify(response)

    with driver.session() as session:
        response = session.write_transaction(add_new_employee, name, surname, position, department)
        return jsonify(response)

#4
def add_department(tx, name, info):
    query = "CREATE (d:department {name: $name, info: $info})"
    tx.run(query, name=name, info=info)

@app.route('/departments', methods=['POST'])
def add_department_route():
    try:
        name = request.json['name']
        info = request.json['info']

    except:
        response = {'status': 'error, all data must be provided'}
        return jsonify(response)


    with driver.session() as session:
        response = session.write_transaction(add_new_employee, name, info)
        return jsonify(response)
#5
def update_employee_by_name(tx, name, surname, position, department):
    query = (
    "MATCH(e:employee {name: $name}) \n"
    "SET e.surname = $surname, e.position = $position \n"
    "WITH e \n"
    "MATCH(e) - [r: WORKS_IN]->(:department) \n"
    "DELETE r \n"
    "WITH e \n"
    "MATCH(d: department {name: $department}) \n"
    "MERGE(e) - [: WORKS_IN]->(d)"
    )
    tx.run(query, name=name, surname=surname, position=position, department=department)



@app.route('/employees/<string:name>', methods=['PUT'])
def update_employee_route(name):
    try:
        surname = request.json['surname']
        position = request.json['position']
        department = request.json.get('department')
    except:
        response = {'status': 'error, all data must be provided'}
        return jsonify(response)
    print(surname)
    with driver.session() as session:
        session.write_transaction(update_employee_by_name, name, surname, position, department)

    response = {'status': 'success'}
    return jsonify(response)
#6
def delete_employee(tx, name, new_manager=None):
    query = "MATCH (e:employee {name: $name})-[r:WORKS_IN]-(d) DETACH DELETE e"
    res = tx.run(query, name=name).data()
    if len(res) == 0:
        if new_manager == None:
            query = "MATCH (e:employee {name: $name})-[r:MANAGES]-(d:department) DETACH DELETE e, d"
            tx.run(query, name=name).data()
        else:
            query = "MATCH (e:employee {name: $name})-[r:MANAGES]-(d:department) DETACH DELETE e WITH d MATCH (n:employee {name: $new_manager}) MERGE (n)-[:MANAGES]->(d)"
            tx.run(query, name=name, new_manager=new_manager).data()


@app.route('/employees/<string:name>', methods=['DELETE'])
def delete_employee_route(name):
    try:
        new_manager = request.json['name']
        with driver.session() as session:
            session.write_transaction(delete_employee, name, new_manager)
    except:
        res = {'status': 'deleting department'}

    with driver.session() as session:
        session.write_transaction(delete_employee, name)

    response={'status': 'success'}
    return jsonify(response)

#7
def all_subordinates(tx, name):
    query = "MATCH (e:employee {name: $name})-[r:MANAGES]-(d:department) WITH d MATCH (sub:employee)-[t:WORKS_IN]-(d) RETURN sub"
    results = tx.run(query, name=name).data()
    employees = [{'name': result['sub']['name'], 'surname': result['sub']['surname'], 'position': result['sub']['position']}
                 for result in results]
    return employees

@app.route('/employees/<string:name>/subordinates', methods=['GET'])
def get_subordinates_route(name):
    with driver.session() as session:
        emp = session.read_transaction(all_subordinates, name)

    response = {'status': emp}
    return jsonify(response)

#8
def get_dep(tx, name):
    query = "MATCH (e:employee {name: $name})-[r]-(d:department) RETURN d"
    results = tx.run(query, name=name).data()
    count_workers = "MATCH (e:employee {name: $name})-[r]-(d:department) WITH d MATCH (i:employee)-[t]-(d) RETURN COUNT(t) as workers"
    co = tx.run(count_workers, name=name).data()
    data = [{'count': result['workers']}
                 for result in co]
    query3 = ("MATCH (e:employee {name: $name})-[r]-(d:department) \n "
        "WITH d \n "
        "MATCH (m:employee)-[:MANAGES]-(d) \n "
        "RETURN m ")
    results3 = tx.run(query3, name=name)
    manager = [{'manager': result['m']['name']}
                 for result in results3]
    employees = [{'name': result['d']['name'],'info': result['d']['info']}
                 for result in results]
    res = [employees, data, manager]
    return res


@app.route('/employees/<string:name>', methods=['GET'])
def get_movie_route(name):
    with driver.session() as session:
        response = session.read_transaction(get_dep, name)
        return jsonify(response)

#9
def get_Department(tx, prop=None, filter=None):
    query = "MATCH (d:department)"

    if filter == "count":
        query += "-[r]-(e:employee)"
    if prop and filter:
        a = f"d.{prop} = {filter}"
        query += " WHERE " + a + " "

    query += " RETURN d "

    results = tx.run(query, prop=prop, filter=filter).data()
    employees = [{'name': result['e']['name'], 'surname': result['e']['surname'], 'position': result['e']['position']}
                 for result in results]
    return employees

@app.route('/employees', methods=['GET'])
def get_department_route():
    prop = request.args.get('prop')
    filterr = request.args.get('filter')
    with driver.session() as session:
        employees = session.read_transaction(get_Department, prop, filterr)

    response = {'employees': employees}
    return jsonify(response)
#10
def departament_workers(tx, name):
    query = "MATCH (d:department {name: $name})-[r]-(e:employee) RETURN e"
    results = tx.run(query, name=name).data()
    employees = [{'name': result['e']['name'], 'surname': result['e']['surname'], 'position': result['e']['position']}
                 for result in results]

    return employees

@app.route('/departments/<string:name>/employees', methods=['GET'])
def get_departments_employees_route(name):
    with driver.session() as session:
        emp = session.read_transaction(departament_workers, name)
    response = {'status': emp}
    return jsonify(response)


if __name__ == '__main__':
    add_data()
    app.run()
